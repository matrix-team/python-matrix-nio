Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: nio
Upstream-Contact: https://github.com/poljar/matrix-nio/issues
Source: https://github.com/poljar/matrix-nio/releases

Files: *
Copyright: 2018-2020, Damir Jelić <poljar@termina.org.uk>
           2020-2021, Famedly GmbH
           2019, miruka <miruka@disroot.org>
License: ISC

Files: src/nio/crypto/attachments.py
       src/nio/crypto/key_export.py
       src/nio/store/database.py
       src/nio/store/models.py
Copyright: 2018-2019, Damir Jelić <poljar@termina.org.uk>
           2018, Zil0
License: Apache-2.0

Files: debian/*
Copyright: 2019-2021, Jonas Smedegaard <dr@jones.dk>
License: GPL-3+

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 <http://www.apache.org/licenses/LICENSE-2.0>
 .
 See /usr/share/common-licenses/Apache-2.0

License: GPL-3+
 This packaging is free software;
 you can redistribute it and/or modify it
 under the terms of the GNU General Public License
 as published by the Free Software Foundation;
 either version 3 of the License, or (at your option) any later version.
 .
 See /usr/share/common-licenses/GPL-3

License: ISC
 Permission to use, copy, modify, and/or distribute this software
 for any purpose with or without fee
 is hereby granted,
 provided that the above copyright notice and this permission notice
 appear in all copies.
 .
 THE SOFTWARE IS PROVIDED "AS IS"
 AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE
 INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS.
 IN NO EVENT SHALL THE AUTHOR BE LIABLE
 FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES
 OR ANY DAMAGES WHATSOEVER
 RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 WHETHER IN AN ACTION OF CONTRACT,
 NEGLIGENCE OR OTHER TORTIOUS ACTION,
 ARISING OUT OF OR IN CONNECTION WITH
 THE USE OR PERFORMANCE OF THIS SOFTWARE.
